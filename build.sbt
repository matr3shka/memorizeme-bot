name := "memorizeme-bot"

version := "0.1"

scalaVersion := "2.12.10"

scalacOptions := List(
  "-encoding",
  "utf8",
  "-feature",
  "-unchecked",
  "-deprecation",
  "-target:jvm-1.8",
  "-language:_",
  "-Ypartial-unification"
)

val telegramVersion = "4.4.0-RC2"
val catsEffectVersion = "2.0.0"
val sttpVersion = "1.7.2"
val logbackVersion = "1.2.3"
val logger4sVersion = "0.3.1"
val doobieVersion = "0.8.6"
val circeVersion = "0.11.1"
val circeGenericExVersion = "0.11.1"
val circeConfigVersion = "0.6.1"

libraryDependencies ++= Seq(
  // TODO Исключить из telegram-core транзитивные зависимости на io.circe
  "com.bot4s" %% "telegram-core" % telegramVersion,
  "org.typelevel" %% "cats-effect" % catsEffectVersion,
  "com.softwaremill.sttp" %% "async-http-client-backend-cats" % sttpVersion,
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "org.pure4s" %% "logger4s-cats" % logger4sVersion,
  "org.tpolecat" %% "doobie-core" % doobieVersion,
  "org.tpolecat" %% "doobie-hikari" % doobieVersion,
  "org.tpolecat" %% "doobie-postgres" % doobieVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-literal" % circeVersion,
  "io.circe" %% "circe-generic-extras" % circeGenericExVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.circe" %% "circe-config" % circeConfigVersion
)