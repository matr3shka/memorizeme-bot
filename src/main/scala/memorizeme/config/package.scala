package memorizeme

import io.circe.Decoder
import io.circe.generic.semiauto._

package object config {
  implicit val dbConnDec: Decoder[DatabaseConnectionsConfig] = deriveDecoder
  implicit val dbDec: Decoder[DatabaseConfig] = deriveDecoder
  implicit val mbDec: Decoder[MemoBotConfig] = deriveDecoder
}
