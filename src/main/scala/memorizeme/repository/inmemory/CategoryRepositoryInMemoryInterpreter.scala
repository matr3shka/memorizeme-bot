package memorizeme
package repository.inmemory

import cats._
import cats.implicits._

import scala.collection.concurrent.TrieMap
import domain.categories.{Category, CategoryRepositoryAlgebra}
import java.lang.System.nanoTime

class CategoryRepositoryInMemoryInterpreter[F[_]: Applicative] extends CategoryRepositoryAlgebra[F] {
  private val cache = new TrieMap[Long, Category]

  def create(category: Category): F[Category] = {
    val id = nanoTime
    val toSave = category.copy(id = id.some)
    cache += (id -> toSave)
    toSave.pure[F]
  }

  def findByNameAndUser(name: String, userId: Long): F[List[Category]] =
    cache.values.filter(c => name == c.name && userId == c.userId).toList.pure[F]

  def findByUser(userId: Long): F[List[Category]] =
    cache.values.filter(userId == _.userId).toList.pure[F]
}

object CategoryRepositoryInMemoryInterpreter {
  def apply[F[_]: Applicative]() = new CategoryRepositoryInMemoryInterpreter[F]()
}
