package memorizeme
package repository.inmemory

import java.lang.System.nanoTime

import cats._
import cats.implicits._

import domain.items.{Item, ItemRepositoryAlgebra}

import scala.collection.concurrent.TrieMap

class ItemRepositoryInMemoryInterpreter[F[_]: Applicative] extends ItemRepositoryAlgebra[F] {
  private val cache = new TrieMap[Long, Item]

  override def create(item: Item): F[Item] = {
    val id = nanoTime
    val toSave = item.copy(id = id.some)
    cache += (id -> toSave)
    toSave.pure[F]
  }

  override def create(items: List[Item]): F[List[Item]] =
    items.traverse(create)

  override def findByCategoryAndUser(categoryId: Long, userId: Long): F[List[Item]] =
    cache.values.filter(item => categoryId == item.categoryId && userId == item.userId).toList.pure[F]

}

object ItemRepositoryInMemoryInterpreter {
  def apply[F[_]: Applicative]() = new ItemRepositoryInMemoryInterpreter[F]()
}
