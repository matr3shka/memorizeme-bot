package memorizeme
package repository.inmemory

import java.lang.System.nanoTime

import cats._
import cats.implicits._
import domain.messages.{Message, MessageRepositoryAlgebra}

import scala.collection.concurrent.TrieMap

class MessageRepositoryInMemoryInterpreter[F[_]: Applicative] extends MessageRepositoryAlgebra[F]{
  private val cache = new TrieMap[Long, Message]

  override def save(message: Message): F[Unit] = {
    val id = nanoTime
    val toSave = message.copy(id = id.some)
    cache += (id -> toSave)
    ().pure[F]
  }

  override def deleteByChat(chatId: Long): F[List[Message]] = {
    val removed = cache.values.filter(chatId == _.chatId).toList
    removed.foreach(msg => msg.id.fold(())(cache.remove(_)))
    removed.pure[F]
  }

}

object MessageRepositoryInMemoryInterpreter {
  def apply[F[_]: Applicative]() = new MessageRepositoryInMemoryInterpreter[F]()
}
