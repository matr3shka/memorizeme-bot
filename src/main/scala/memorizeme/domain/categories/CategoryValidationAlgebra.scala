package memorizeme.domain
package categories

import cats.data.EitherT

trait CategoryValidationAlgebra[F[_]] {

  /* Fails with a CategoryAlreadyExistsError */
  def doesNotExist(category: Category): EitherT[F, CategoryAlreadyExistsError, Unit]
}
