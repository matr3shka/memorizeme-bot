package memorizeme.domain
package categories

import cats.Applicative
import cats.data.EitherT
import cats.implicits._

class CategoryValidationInterpreter[F[_]: Applicative](repository: CategoryRepositoryAlgebra[F])
    extends CategoryValidationAlgebra[F] {

  def doesNotExist(category: Category): EitherT[F, CategoryAlreadyExistsError, Unit] = EitherT {
    repository.findByNameAndUser(category.name, category.userId).map {
      case Nil => Right(())
      case _ => Left(CategoryAlreadyExistsError(category))
    }
  }
}

object CategoryValidationInterpreter {
  def apply[F[_]: Applicative](repository: CategoryRepositoryAlgebra[F]) =
    new CategoryValidationInterpreter[F](repository)
}
