package memorizeme.domain.categories

trait CategoryRepositoryAlgebra[F[_]] {

  def create(category: Category): F[Category]

  def findByNameAndUser(name: String, userId: Long): F[List[Category]]

  def findByUser(userId: Long): F[List[Category]]
}
