package memorizeme.domain
package categories

import cats._
import cats.data._
import cats.implicits._

class CategoryService[F[_]](
    repository: CategoryRepositoryAlgebra[F],
    validation: CategoryValidationAlgebra[F],
) {
  def create(category: Category)(implicit M: Monad[F]): EitherT[F, CategoryAlreadyExistsError, Category] =
    for {
      _ <- validation.doesNotExist(category)
      saved <- EitherT.liftF(repository.create(category))
    } yield saved

  def findByNameAndUser(name: String, userId: Long)(implicit F: Functor[F]): EitherT[F, CategoryNotFoundError.type, Category] = EitherT {
    repository.findByNameAndUser(name, userId).map {
      case Nil => Left(CategoryNotFoundError)
      case found: List[Category] => Right(found.head)
    }
  }

  def findByUser(userId: Long): F[List[Category]] =
    repository.findByUser(userId)
}

object CategoryService {
  def apply[F[_]](
    repository: CategoryRepositoryAlgebra[F],
    validation: CategoryValidationAlgebra[F],
  ): CategoryService[F] =
    new CategoryService[F](repository, validation)
}
