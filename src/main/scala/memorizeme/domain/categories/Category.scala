package memorizeme.domain.categories

case class Category (
    id: Option[Long] = None,
    userId: Long,
    name: String
)
