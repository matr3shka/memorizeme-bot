package memorizeme.domain

import categories.Category

sealed trait ValidationError extends Product with Serializable

case class CategoryAlreadyExistsError(category: Category) extends ValidationError
case object CategoryNotFoundError extends ValidationError
