package memorizeme.domain.messages

class MessageService[F[_]](repository: MessageRepositoryAlgebra[F]) {

  def stash(message: Message): F[Unit] =
    repository.save(message)

  def unstash(chatId: Long): F[List[Message]] = {
    repository.deleteByChat(chatId)
  }
}

object MessageService {
  def apply[F[_]](repository: MessageRepositoryAlgebra[F]): MessageService[F] =
    new MessageService[F](repository)
}
