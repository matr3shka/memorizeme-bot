package memorizeme.domain.messages

import com.bot4s.telegram.models.{Message => TelegramMessage}

case class Message(
  id: Option[Long],
  chatId: Long,
  value: TelegramMessage
)
