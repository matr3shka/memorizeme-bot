package memorizeme.domain.messages

trait MessageRepositoryAlgebra[F[_]] {

  def save(message: Message): F[Unit]

  def deleteByChat(chatId: Long): F[List[Message]]
}
