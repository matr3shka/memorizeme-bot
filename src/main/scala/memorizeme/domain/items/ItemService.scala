package memorizeme.domain
package items

import categories.Category
import cats._
import cats.implicits._

class ItemService[F[_]: Applicative](repository: ItemRepositoryAlgebra[F]) {

  def create(item: Item): F[Item] =
    repository.create(item)

  def create(items: List[Item]): F[List[Item]] =
    repository.create(items)

  def findByCategoryAndUser(category: Category, userId: Long): F[List[Item]] =
    category.id match {
      case Some(categoryId) => repository.findByCategoryAndUser(categoryId, userId)
      case _ => List.empty[Item].pure[F]
    }
}

object ItemService {
  def apply[F[_]: Applicative](repository: ItemRepositoryAlgebra[F]): ItemService[F] =
    new ItemService[F](repository)
}
