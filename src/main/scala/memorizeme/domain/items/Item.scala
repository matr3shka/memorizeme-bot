package memorizeme.domain.items

import com.bot4s.telegram.models.{Message => TelegramMessage}

case class Item (
  id: Option[Long] = None,
  userId: Long,
  categoryId: Long, // TODO Может Option и апдейтить?
  message: TelegramMessage
)
