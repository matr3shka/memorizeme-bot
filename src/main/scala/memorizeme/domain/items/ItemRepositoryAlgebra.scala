package memorizeme.domain.items

trait ItemRepositoryAlgebra[F[_]] {

  def create(item: Item): F[Item]

  def create(items: List[Item]): F[List[Item]]

  def findByCategoryAndUser(categoryId: Long, userId: Long): F[List[Item]]
}
