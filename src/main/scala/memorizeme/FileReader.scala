package memorizeme

import java.io._
import cats.effect._

object FileReader {

  def read(file: File): IO[String] = {
    val acquire = IO {
      scala.io.Source.fromFile(file)
    }
    Resource.fromAutoCloseable(acquire).use(source => IO(source.mkString))
  }
}
