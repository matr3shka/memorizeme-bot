package memorizeme

import domain.categories.{CategoryService, CategoryValidationInterpreter}
import memorizeme.config.MemoBotConfig
import repository.inmemory.{CategoryRepositoryInMemoryInterpreter, ItemRepositoryInMemoryInterpreter, MessageRepositoryInMemoryInterpreter}
import java.io.File

import cats.effect.{ExitCode, IO, IOApp}
import org.pure4s.logger4s.LazyLogging
import org.pure4s.logger4s.cats.Logger
import io.circe.config.parser
import memorizeme.domain.items.ItemService
import memorizeme.domain.messages.MessageService

object Launcher extends IOApp with LazyLogging {

  def startBot(token: String): IO[Unit] = for {
    _ <- Logger[IO].info("Starting Memo Bot...")
    conf <- parser.decodePathF[IO, MemoBotConfig]("memobot")
    _ <- Logger[IO].info(s"With configuration $conf")
    help <- FileReader.read(new File(getClass.getResource("/help").getFile))
    categoryRepository = CategoryRepositoryInMemoryInterpreter[IO]
    categoryValidation = CategoryValidationInterpreter[IO](categoryRepository)
    categoryService = CategoryService[IO](categoryRepository, categoryValidation)
    itemRepository = ItemRepositoryInMemoryInterpreter[IO]
    itemService = ItemService[IO](itemRepository)
    messageRepository = MessageRepositoryInMemoryInterpreter[IO]
    messageService = MessageService[IO](messageRepository)
    _ <- new MemoBot(token, help, categoryService, itemService, messageService).startPolling
  } yield ()

  def run(args: List[String]): IO[ExitCode] = {
    args match {
      case List(token) => {
        implicit val instance: Logger[IO] = Logger.instance[IO](classOf[MemoBot[IO]])
        startBot(token).map(_ => ExitCode.Success)
      }
      case _ =>
        IO.raiseError(new Exception("Usage:\nLauncher $token"))
    }
  }
}
