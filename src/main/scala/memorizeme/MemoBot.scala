package memorizeme

import cats.effect.{Async, ContextShift}
import memorizeme.domain.CategoryNotFoundError
import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.syntax.apply._
import com.bot4s.telegram.api.declarative.Commands
import com.bot4s.telegram.cats.Polling
import com.bot4s.telegram.cats.TelegramBot
import com.bot4s.telegram.methods._
import com.bot4s.telegram.models.InputFile.FileId
import com.bot4s.telegram.models._
import com.softwaremill.sttp.asynchttpclient.cats.AsyncHttpClientCatsBackend
import org.pure4s.logger4s.cats.Logger
import domain.categories.{Category, CategoryService}
import domain.items._
import domain.messages.{MessageService, Message => DomainMessage}

class MemoBot[F[_] : Async : ContextShift : Logger] (
  token: String,
  help: String,
  categoryService: CategoryService[F],
  itemService: ItemService[F],
  messageService: MessageService[F]
) extends TelegramBot(token, AsyncHttpClientCatsBackend())
  with Polling[F]
  with Commands[F] {

  import MemoBotCommands._
  import MemoBotReplies._

  override def receiveMessage(msg: Message): F[Unit] = {
    logMessage(msg) *> {
      if (isSupportedMessage(msg)) {
        msg.text match {
          case None => messageService.stash(DomainMessage(None, msg.chat.id, msg)).void
          case Some(text) if notContainsCommand(text) => {
            messageService.stash(DomainMessage(None, msg.chat.id, msg))
//            request(SendMessage(msg.source, s"Re: $text")).void
          }
          case _ => unit
        }
      } else {
        request(SendMessage(msg.source, UNSUPPORTED_MESSAGE)).void
      }
    }
  }

  onCommand(START | HELP) { implicit msg =>
    logCommand(msg) *>
    reply(help).void
  }

  onCommand(SAVE) { implicit msg =>
    logCommand(msg) *>
      msg.text.fold(unit) { text =>
        val categoryName = text.substring(text.indexOf(SAVE) + 5).trim
        if (categoryName.isEmpty) {
          reply(NO_CATEGORY_NAME_TO_SAVE).void
        } else {
          msg.from.fold(unit)(saveStashedMessages(_, categoryName))
        }
      }
  }

  onCommand(SHOW) { implicit msg =>
    logCommand(msg) *>
      msg.text.fold(unit) { text =>
        val categoryName = text.substring(text.indexOf(SHOW) + 5).trim
        if (categoryName.isEmpty) {
          reply(NO_CATEGORY_NAME_TO_SHOW).void
        } else {
          msg.from.fold(unit)(showCategoryContent(_, categoryName))
        }
      }
  }

  onCommand(NEW) { implicit msg =>
    logCommand(msg) *>
    msg.text.fold(unit) { text =>
      val categoryName = text.substring(text.indexOf(NEW) + 4).trim
      if (categoryName.isEmpty) {
        reply(NO_CATEGORY_NAME_TO_CREATE).void
      } else {
        msg.from.fold(unit)(createNewCategory(_, categoryName))
      }
    }
  }

  onCommand(ALL) { implicit msg =>
    logCommand(msg) *>
    msg.from.fold(unit)(showAllCategories)
  }

  onCommand(EDIT | DROP) { implicit msg =>
    logCommand(msg) *>
      reply(NOT_IMPLEMENTED_COMMAND).void
  }

  def saveStashedMessages(user: User, categoryName: String)(implicit msg: Message): F[Unit] = {
    val categoryEither = categoryService.findByNameAndUser(categoryName, user.id).value
    categoryEither.flatMap {
      case Left(CategoryNotFoundError) => reply(CATEGORY_NOT_FOUND(categoryName)).void
      case Right(category) => {
        val stashed = messageService.unstash(msg.chat.id)
        stashed.flatMap {
          case Nil => reply(NO_MESSAGES_TO_SAVE).void
          case messages => {
            category.id.fold(unit) { categoryId =>
              val items = messages.map(message => Item(None, user.id, categoryId, message.value))
              itemService.create(items).void
            }
          }
        }
      }
    }
  }

  def showCategoryContent(user: User, categoryName: String)(implicit msg: Message): F[Unit] = {
    val categoryEither = categoryService.findByNameAndUser(categoryName, user.id).value
    categoryEither.flatMap {
      case Left(CategoryNotFoundError) => reply(CATEGORY_NOT_FOUND(categoryName)).void
      case Right(category) => {
        val items = itemService.findByCategoryAndUser(category, user.id)
        items.flatMap {
          case Nil => reply(NO_MESSAGES_TO_SHOW(categoryName)).void
          case values => values.foldLeft(unit)((acc, item) => acc.flatMap(_ => showItem(item, msg.source)))
        }
      }
    }
  }

  def showItem(item: Item, chatId: ChatId): F[Unit] = {
    sendRequest(item.message, chatId).fold(unit)(request(_).void)
  }

  def sendRequest(message: Message, chatId: ChatId): Option[Request[Message]] = message match {
//    case msg if msg.forwardFromChat != None && msg.forwardFromMessageId != None =>
//      for {
//        fromChat <- msg.forwardFromChat
//        fromMsgId <- msg.forwardFromMessageId
//      } yield ForwardMessage(chatId, ChatId(fromChat.id), None, fromMsgId)
    case msg if msg.text != None => msg.text.map(text => Some(SendMessage(chatId, text))).getOrElse(None)
    case msg if msg.photo != None => msg.photo.map(photo => Some(SendPhoto(chatId, FileId(photo.head.fileId)))).getOrElse(None)
    case msg if msg.sticker != None => msg.sticker.map(sticker => Some(SendSticker(chatId, FileId(sticker.fileId)))).getOrElse(None)
    case msg if msg.audio != None => msg.audio.map(audio => Some(SendAudio(chatId, FileId(audio.fileId)))).getOrElse(None)
    case msg if msg.animation != None => msg.animation.map(animation => Some(SendAnimation(chatId, FileId(animation.fileId)))).getOrElse(None)
    case _ => None
  }

  def createNewCategory(user: User, name: String)(implicit msg: Message): F[Unit] = {
    val category = Category(None, user.id, name)
    val result = categoryService.create(category).value
    result.flatMap {
      case Right(value) => reply(CATEGORY_ADDED(value.name)).void
      case Left(error) => reply(CATEGORY_EXISTS(error.category.name)).void
    }
  }

  def showAllCategories(user: User)(implicit msg: Message): F[Unit] = {
    val categories = categoryService.findByUser(user.id).map(_.map(_.name)).map {
      case Nil => NO_SAVED_CATEGORIES
      case names => names.mkString("\n")
    }
    categories.flatMap(text => reply(text)).void
  }

  def logMessage(msg: Message): F[Unit] =
     Logger[F].info(s"Message received from user ${msg.from.fold("unknown")(_.id.toString)}")

  def logCommand(msg: Message): F[Unit] =
    Logger[F].info(s"Command received from user ${msg.from.fold("unknown")(_.id.toString)}: ${msg.text.getOrElse("null")}")

  def isSupportedMessage(msg: Message): Boolean = {
    msg.captionEntities.isEmpty && (
      msg.text.nonEmpty
        || msg.photo.nonEmpty
        || msg.sticker.nonEmpty
        || msg.audio.nonEmpty
        || msg.animation.nonEmpty
//        || msg.voice.nonEmpty
//        || msg.document.nonEmpty
      )
  }

  def notContainsCommand(text: String): Boolean = {
    COMMANDS.find(text.indexOf(_) != -1).isEmpty
  }
}

private object MemoBotCommands {
  val START = "/start"
  val HELP = "/help"

  val NEW = "/new"
  val ALL = "/all"

  val SAVE = "/save"
  val SHOW = "/show"

  val DROP = "/drop"
  val EDIT = "/edit"

  val COMMANDS = Set(START, HELP, NEW, ALL, SAVE, SHOW, DROP, EDIT)
}

private object MemoBotReplies {
  val NO_CATEGORY_NAME_TO_CREATE = "Укажи название категории, которую хочешь добавить"
  val NO_CATEGORY_NAME_TO_SAVE = "Укажи название категории, в которую хочешь сохранить сообщения"
  val NO_CATEGORY_NAME_TO_SHOW = "Укажи название категории, которую хочешь просмотреть"

  def NO_MESSAGES_TO_SHOW(name: String) = s"В категории $name пока нет сообщений"
  val NO_MESSAGES_TO_SAVE = "Нет несохраненных сообщений"

  def CATEGORY_NOT_FOUND(name: String) = s"Категория '$name' еще не добавлена"
  def CATEGORY_ADDED(name: String) = s"Категория '$name' добавлена"
  def CATEGORY_EXISTS(name: String) = s"Категория '$name' уже была добавлена ранее"

  def MESSAGES_SAVED(name: String) = s"Сообщения сохранены в '$name'"

  val NO_SAVED_CATEGORIES = "У тебя пока нет ни одной категории"

  val UNSUPPORTED_MESSAGE = "Ой... Я пока не знаю, что делать с этим сообщением :("

  val UNKNOWN_COMMAND = "Ой... Я такую команду не знаю :|"
  val INVALID_COMMAND = "Ой... Чтобы я выполнил команду, она должна быть первым словом в сообщении"
  val NOT_IMPLEMENTED_COMMAND = "Ой... Когда-нибудь я научусь это делать :)"
}